﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _461toolsfinalVSproj
{
    class BaseController:EventListener
    {
        public BaseController()
        {
            View view1 = new View();
            view1.dostuff();
        }

        public void ProcessEvent()
        {
            Model model = new Model();
            model.VersionID = 1;
        }
    }
}
